#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import socket
import json
import os
import binascii
import hashlib

import client.client_json

MAX_RECV = 1024 * 1024 * 512

HOST = ''


def verifie_syntaxe(msgApp):
    "Analyse la syntaxe JSON du message du serveur"
    reponse = True
    try:
        json.loads(msgApp)
    except:
        reponse = False

    return reponse


### Main ###
if __name__ == '__main__':

    if len(sys.argv) <= 2:
        print("Ce client prend l'adresse IP et le numéro du port en paramètre.")
        print("Si le troisième paramètre est \"--archive\", le client demande")
        print("une archive ZIP de l'arborescence (fichier archive.zip).")
        print("Le quatrième paramètre est le port de réception de l'archive.")
        sys.exit(1)

    # Récupérer l'adresse IP en paramètre du serveur.
    host = sys.argv[1]

    # Récurérer le numéro du port en paramètre du serveur.
    port = int(sys.argv[2])
    port_archive = port

    archivage = False
    if len(sys.argv) > 3:
        if sys.argv[3] == '--archive':
            archivage = True
            port_archive = sys.argv[4]

    racine = './'


    if not archivage:

        print("Synchronisation de l'arborescence pour le dossier '" + racine + "'...")
        conn_client = client.client_json.Client(host, port)
        conn_client.miseAjour(racine)
        print("Synchronisation terminée.")
        sys.exit(0)
    else:
        print("Demande d'archive ZIP de l'arborescence...")
        client = socket.socket()
        client.connect((host, port))
        json_archivage = {"archive": ""}
        texte_archivage = json.dumps(json_archivage) + "\r\n"
        client.sendall(texte_archivage.encode(encoding='UTF-8'))
        client.close()

        print("Attente de réception de l'archive...")
        # if wait_net_service('', port):
        serveur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serveur.bind(('', int(port_archive)))

        serveur.listen(5)
        connexion, adresse = serveur.accept()

        compteur = 1
        texte_recu_total = ''
        MAX_NOMBRE_RECV = 100
        while compteur < MAX_NOMBRE_RECV:
            print('Attente du contenu... (' + str(compteur) + '/' + str(MAX_NOMBRE_RECV) + ')')
            texte_recu = connexion.recv(MAX_RECV).decode(encoding='UTF-8')
            compteur += 1
            texte_recu_total += texte_recu
            if verifie_syntaxe(texte_recu_total):
                break
            else:
                print("Attention, le texte reçu jusqu'à maintenant n'est pas syntaxiquement correct.")
                print("Attente de la suite...")
                print("(Faire CTRL-C s'il y a erreur)")
        if compteur >= MAX_NOMBRE_RECV:
            print("Réponse définitive: le texte du serveur reçu n'est pas syntaxiquement correct.")
            serveur.close()
            sys.exit(1)

        print("Réception terminée. Enregistrement de l'archive...")
        serveur.close()

        signature = ''
        contenu = ''
        date = ''
        fichier = './archive.zip'
        dom = json.loads(texte_recu_total.decode('utf-8'))
        for node in dom:
            if node == 'fichier':
                node_interne = dom[node]
                for node2 in node_interne:
                    if node2 == 'signature':
                        signature = node_interne[node2]
                    if node2 == 'contenu':
                        contenu = node_interne[node2]
                    if node2 == 'date':
                        date = node_interne[node2]

        # Conversion du contenu
        try:
            contenu_decode = binascii.a2b_base64(contenu)
        except:
            contenu += '=' * (-len(contenu) % 4)
            contenu_decode = binascii.a2b_base64(contenu)

        if signature != hashlib.sha256(contenu).hexdigest():
            print("Erreur: le fichier d'archive n'a pas une bonne signature...")
            sys.exit(1)
        else:
            try:
                obj_fichier = open(fichier, 'wb')
                obj_fichier.write(contenu_decode)
                obj_fichier.close()

                try:
                    fichier_stat = os.stat(fichier)
                    date_acces = fichier_stat.st_atime
                    os.utime(fichier, (date_acces, float(date)))
                except OSError:
                    print("Erreur: impossible d'écrire la date " + str(date) + " du fichier " + fichier)
                    sys.exit(1)

            except IOError:
                # TODO: Ajouter une erreur de type "Erreur fichier ecriture"
                print("Erreur: impossible d'écrire le fichier \"" + fichier + "\" sur le système de fichier.")
                sys.exit(1)
