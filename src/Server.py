from src.Connection import Connection
import socket

class Server:
    def __init__(self, address, command_manager):
        self.__socket = socket.socket()
        self.__socket.bind(address)
        self.__socket.listen()
        self.__command_manager = command_manager

    def start(self):
        try:
            while True:
                Connection(self.__socket.accept()[0], self.__command_manager).start()
        except:
            self.__socket.close()
