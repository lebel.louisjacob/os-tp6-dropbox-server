#!/usr/bin/python3
# -*- coding: utf-8 -*-
import json
from FileSystem import FileSystem


class Client:

    def __init__(self, tcpip):
        self.tcpip = tcpip
        self.file_system = FileSystem()

    def split_path(self, path_to_file):
        parts = path_to_file.split('/')
        file = parts[-1]
        path = '/'.join(parts[:-1])
        return file, path

    def to_json(self, dict):
        return json.dumps(dict)

    #########################################################################
    ############################### I N F O #################################
    #########################################################################
    def salutation(self):
        self.tcpip.send({"salutation": "bonjourServeur"})
        return self.tcpip.receive()['salutation']

    def nom_serveur(self):
        self.tcpip.send({"questionNomServeur": ""})
        return self.tcpip.receive()['nomServeur']

    def connecter(self):
        return 'Oui' if(self.tcpip.is_connected) else 'Non'


    #########################################################################
    ############################# F O L D E R ###############################
    #########################################################################
    def creer_dossier(self, folder):
        self.tcpip.send({"creerDossier": folder})
        return self.tcpip.receive()['reponse']

    def supprimer_dossier(self, folder):
        self.tcpip.send({"supprimerDossier": folder})
        return self.tcpip.receive()['reponse']

    def liste_dossiers(self, path):
        self.tcpip.send({"questionListeDossiers": path})
        response = self.tcpip.receive()
        try:
            return response['listeDossiers']["dossiers"]
        except KeyError:
            return response['reponse']

    def dossier_existant(self, folder):
        return "non" if "erreurDossierInexistant" in str(self.liste_dossiers(folder)) else "oui"

    #########################################################################
    ############################### F I L E S ###############################
    #########################################################################
    def televerser_fichier(self, path_to_file):
        file, path = self.split_path(path_to_file)
        self.tcpip.send({"televerserFichier": {
                "nom": file,
                "dossier": path,
                "signature": self.file_system.get_file_signature(path_to_file),
                "contenu": self.file_system.get_file_content(path_to_file),
                "date": str(self.file_system.get_file_date(path_to_file))
            }
        })
        return self.tcpip.receive()["reponse"]

    def telecharger_fichier(self, path_to_file):
        name, path = self.split_path(path_to_file)
        self.tcpip.send({"telechargerFichier": {
                "nom": name,
                "dossier": path
            }
        })
        encoded_response = self.tcpip.receive()
        try:
            self.file_system.make_file_content(path_to_file, encoded_response['fichier'])
            return "ok"
        except KeyError:
            return encoded_response["reponse"]

    def supprimer_fichier(self, path_to_file):
        name, path = self.split_path(path_to_file)
        self.tcpip.send({"supprimerFichier":{
                "nom": name,
                "dossier": path
            }
        })
        return self.tcpip.receive()["reponse"]

    def liste_fichiers(self, path):
        self.tcpip.send({"questionListeFichiers": path})
        reponse = self.tcpip.receive()
        try:
            return reponse["listeFichiers"]["fichiers"]
        except KeyError:
            return reponse["reponse"]


    def fichier_existant(self, path):
        file, path = self.split_path(path)
        return "oui" if file in self.liste_fichiers(path) else "non"

    def fichier_identique(self, path_to_file):
        file, path = self.split_path(path_to_file)
        signature = self.file_system.get_file_signature(path_to_file)
        date = str(self.file_system.get_file_date(path_to_file))
        self.tcpip.send({"questionFichierIdentique":{
            "nom": file,
            "dossier": path,
            "signature": signature,
            "date": date
            }
        })
        return self.tcpip.receive()["reponse"]

    def fichier_recent(self, path_to_file):
        file, path = self.split_path(path_to_file)
        date = str(self.file_system.get_file_date(path_to_file))
        self.tcpip.send({"questionFichierRecent":{
            "nom": file,
            "dossier": path,
            "date": date
            }
        })
        return self.tcpip.receive()["reponse"]

    #################################################################################
    ######################### M I S E - A - J O U R #################################
    #################################################################################
    def mise_a_jour(self, path):
        liste = self.liste_fichiers(path)
        for file in liste:
            path_to_file = path+'/'+file
            if self.fichier_recent(path_to_file) == 'Oui':
                print('...')
                self.supprimer_fichier(path_to_file)
                self.televerser_fichier(path_to_file)
            if self.fichier_recent(path_to_file) == 'Non':
                print('...')
                self.telecharger_fichier(path_to_file)
        print("Mise a jour complete")

    #########################################################################
    ################################# Q U I T ###############################
    #########################################################################
    def quitter(self):
        self.tcpip.send({"quitter": ""})
        return self.tcpip.receive()["reponse"]

