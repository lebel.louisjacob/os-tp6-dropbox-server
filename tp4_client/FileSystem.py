#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import hashlib
import binascii
import sys
import math


class FileSystem:

    def __init__(self):
        pass

    def ouvrir(self, fichier, mode='r'):
        return open(fichier, mode)

    def fermer(self, fichier):
        fichier.close()

    def dossier_existe(self, dossier):
        return os.path.exists(dossier)

    def fichier_existe(self, fichier):
        return os.path.exists(fichier)

    def fichier_accessible(self, file, for_mode=os.R_OK):
        return os.access(file, for_mode)

    def liste_dossiers(self, dossier):
        liste_dossiers = list()
        liste_objets = os.listdir(dossier)

        for nom in liste_objets:
            if os.path.isdir(dossier + nom):
              liste_dossiers.append(nom)
        return liste_dossiers

    def liste_fichiers(self, dossier):
        liste_fichiers = list()
        liste_objets = os.listdir(dossier)
        for nom in liste_objets:
            if os.path.isfile(dossier + nom):
                liste_fichiers.append(nom)
        return liste_fichiers

    def creer_dossier(self, dossier):
        return os.mkdir(dossier)

    def get_file_content(self, file_name):
        return binascii.b2a_base64(open(file_name, 'rb').read()).decode('utf-8')

    def make_file_content(self, pathToFile, file_data):
        content = file_data['contenu']
        open(pathToFile, 'wb').write(binascii.a2b_base64(content.encode('utf-8')))

    def get_file_signature(self, file):
        try:
            read_content = self.ouvrir(file, mode='rb').read()
        except:
            print(
                "Erreur de lecture du fichier '" + file +
                "' pour obtention de la signature...")
            sys.exit(1)
        m = hashlib.sha256()
        m.update(read_content)
        return m.hexdigest()

    def get_file_date(self, file):
        try:
            timestamp = os.path.getmtime(file)
        except:
            print(
                "Erreur de lecture du fichier '" + file +
                "' pour obtention de la date...")
            sys.exit(1)
        return timestamp

    def fix_date(self, file, date_modif):
        try:
            file_stat = os.stat(file)
            date_acces = file_stat.st_atime
            os.utime(file, (date_acces, date_modif))
        except:
            print("Erreur: impossible d'écrire la date" + str(date_modif) + "du fichier" + file)
            sys.exit(1)

    def dates_plus_recentes(self, client_date, server_date):
        if math.floor(float(client_date)) > math.floor(server_date):
            reponse = True
        else:
            reponse = False
        return reponse