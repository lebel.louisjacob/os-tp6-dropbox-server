class ClientSocketMock:
    def __init__(self):
        self.__recv = recv

    def recv(self):
        return self.__recv

    def send(self, data):
        self.sent = data

    def close(self):
        pass
