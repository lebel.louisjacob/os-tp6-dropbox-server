import socket, json, threading, traceback
from src.CloseConnectionException import CloseConnectionException

class Connection(threading.Thread):
    END_OF_STREAM = '\r\n'
    BUFFER_SIZE = 1024 * 1024 * 512
    ARCHIVE_PORT = 5001

    def __init__(self, socket, command_manager):
        super().__init__()
        self.__socket = socket
        self.__address, self.__port = socket.getpeername()
        self.__command_manager = command_manager
        print('connected')

    def run(self):
        try:
            while True:
                client_recv = self.recv()
                key = list(client_recv.keys())[0]
                body = list(client_recv.values())[0]
                command = self.__command_manager(key)
                if key == 'archive':
                    self.__socket.close()
                    self.__socket = socket.socket()
                    self.__socket.connect((self.__address, Connection.ARCHIVE_PORT))
                    key = 'quitter'
                self.send(command(body))

                if key == 'quitter':
                    raise CloseConnectionException()
        except CloseConnectionException:
            print('connection closed')
        except Exception as e:
            print(traceback.format_exc())
            print(e)
        finally:
            self.close()

    def close(self):
        self.__socket.close()

    def send(self, obj):
        self.__send_raw(json.dumps(obj))

    def recv(self):
        return json.loads(self.__recv_raw())

    def __send_raw(self, data, end_of_stream=END_OF_STREAM):
        self.__socket.send((data + end_of_stream).encode('UTF-8'))

    def __recv_raw(self, buffer_size=BUFFER_SIZE, end_of_stream=END_OF_STREAM):
        return self.__socket.recv(buffer_size).decode('UTF-8')
