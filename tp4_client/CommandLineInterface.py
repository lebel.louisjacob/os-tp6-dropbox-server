#!/usr/bin/python3
# -*- coding: utf-8 -*-

class CommandLineInterface:

    def __init__(self, client, console):
        self.client = client
        self.console = console
        self.methods_no_args = {
            "nomServeur?": self.client.nom_serveur,
            "connecter?": self.client.connecter,
            "quitter": self.client.quitter,
        }
        self.methods_args = {
            "listeDossier?": self.client.liste_dossiers,
            "creerDossier?": self.client.creer_dossier,
            "supprimerDossier?": self.client.supprimer_dossier,
            "dossier?": self.client.dossier_existant,
            "fichier?": self.client.fichier_existant,
            "telecharger?": self.client.telecharger_fichier,
            "televerser?": self.client.televerser_fichier,
            "supprimerFichier?": self.client.supprimer_fichier,
            "fichierIdentique?": self.client.fichier_identique,
            "identiqueFichier?": self.client.fichier_identique,
            "fichierRecent?": self.client.fichier_recent,
            "recentFichier?": self.client.fichier_recent,
            "listeFichiers?": self.client.liste_fichiers,
            "miseAjour?": self.client.mise_a_jour
        }
        self.enter_command()

    def get_commands(self):
        return input(">>>").split()

    def enter_command(self):
        commands = self.get_commands()
        if len(commands) < 2:
            self.method_facto(commands[0])
        elif len(commands) == 2:
            method, path = commands
            self.method_with_arg_facto(method, path)
        else:
            print("Vous avez passer trop de parametre, ressayer avec 1 seulement")

        if commands[0] != 'quitter':
            self.enter_command()

    def method_facto(self, method):
        try:
            self.console.print_it(self.methods_no_args[method]())
        except KeyError:
            self.console.print_command_error_message()

    def method_with_arg_facto(self, method, path):
        try:
            self.console.print_it(self.methods_args[method](path))
        except KeyError:
            self.console.print_command_error_message()