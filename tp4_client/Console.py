#!/usr/bin/python3
# -*- coding: utf-8 -*-
import json
from FileSystem import FileSystem


class Console():

    def __init__(self):
        self.message = {
            "erreurDossierExiste": "Dossier existe déjà.",
            "erreurDossierInexistant": "Dossier n'existe pas.",
            "erreurFichierInexistant": "Fichier n'existe pas.",
            "erreurFichierExiste": "Fichier existe déjà.",
        }

    def print_it(self, content):
        if content:
            if type(content) is list:
                for element in content:
                    print(element)
            else:
                try:
                    print(self.message[content])
                except KeyError:
                    print(content)
    def print_command_error_message(self):
        print('methode inexistance ou parametre insuffisant')