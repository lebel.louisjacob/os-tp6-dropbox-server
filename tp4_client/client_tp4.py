#!/usr/bin/python3
# -*- coding: utf-8 -*-
from Tcpip import Tcpip
from Client import Client
from CommandLineInterface import CommandLineInterface
from Console import Console
import socket
import sys

host = '127.0.0.1'
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.settimeout(None)

port = int(sys.argv[1])
tcpip = Tcpip(host, port)
client = Client(tcpip)

tcpip.connect()

if len(sys.argv) == 3 and sys.argv[2] == 'prompt':
    cmd = CommandLineInterface(client, Console())
    sys.exit()

#test all method with this scenario
folder = "."

def mise_a_jour_des_fichiers():
    liste_fichier = client.liste_fichiers(folder)
    for fichier in liste_fichier:
        if fichier[:1] == '.' or fichier == "serveur_tp4.pyc":
            print('On ne fera pas de mise-a-jour sur :'+fichier)
        else:
            print('des mise a jours seront applique sur :'+fichier)
            client.mise_a_jour(fichier)

def mise_a_jour_des_dossiers():
    liste_dossier = client.liste_dossiers(folder)
    for dossier in liste_dossier:
        if dossier[:1] == '.':
            print('On ne fera pas de mise-a-jour sur :'+dossier)
        else:
            print('des mise a jours seront applique sur :'+dossier)
            client.mise_a_jour(dossier)
            

mise_a_jour_des_fichiers()
mise_a_jour_des_dossiers()